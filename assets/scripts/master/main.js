var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		MODALS
	\*----------------------------------------------------------------*/
	MicroModal.init();
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		COOKIE & GLOBAL NOTIFICATION
	\*----------------------------------------------------------------*/
	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	if (readCookie('globalNotification') === 'false') {
		$('.global-notification').removeClass("note-on");
	} else {
		$('.global-notification').addClass("note-on");
	}
	$('.global-notification button').click(function () {
		$('.global-notification').removeClass("note-on");
		createCookie('globalNotification', 'false');
	});
	$('.global-notification a').click(function () {
		$('.global-notification').removeClass("note-on");
		createCookie('globalNotification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
});
/*----------------------------------------------------------------*\
	GALLERY SLIDER
\*----------------------------------------------------------------*/
$('.gallery.is-slider').slick({
	arrows: false,
	dots: true,
	slidesToShow: 3,
	slidesToScroll: 1,
	centerMode: true,
	centerPadding: '60px',
	autoplay: true,
  autoplaySpeed: 3000,
	responsive: [
    {
      breakpoint: 1050,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
});
/*----------------------------------------------------------------*\
	GALLERY VARIABLE WIDTH SLIDER
\*----------------------------------------------------------------*/
$('.gallery.is-variable-slider').slick({
	arrows: false,
	dots: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	centerMode: true,
	variableWidth: true,
	autoplay: true,
	autoplaySpeed: 3000
});
/*----------------------------------------------------------------*\
	ARTISTS SLIDER
\*----------------------------------------------------------------*/
$('.artist-slider').slick({
	arrows: false,
	dots: true,
	slidesToShow: 5,
	slidesToScroll: 1,
	centerMode: true,
	centerPadding: '60px',
	autoplay: true,
  autoplaySpeed: 2500,
	responsive: [
		{
      breakpoint: 1500,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
      }
		},
		{
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
});
/*----------------------------------------------------------------*\
	HOME SLIDER
\*----------------------------------------------------------------*/
$('.front-page-slider .slides').slick({
	arrows: false,
	asNavFor: '.nav-slides',
	autoplay: true,
  autoplaySpeed: 3000,
});

$('.front-page-slider .nav-slides').slick({
	arrows: false,
	slidesToShow: 5,
	asNavFor: '.slides',
	focusOnSelect: true,
	autoplay: true,
  autoplaySpeed: 3000,
});

/*----------------------------------------------------------------*\
	MEGA MENUS
\*----------------------------------------------------------------*/
$('.toggle-visit-mega').on('click', function(e) {
	if ( $('.mega-menu:not(.visit-mega)').hasClass("is-active") ) {
		//nothing
	} else {
		$('body').toggleClass('no-scroll');
	}
	$('.toggle-mega:not(.toggle-visit-mega)').removeClass("is-active");
	$('.mega-menu:not(.visit-mega)').removeClass("is-active");
	$(this).toggleClass("is-active");
	$('.visit-mega').toggleClass("is-active");
});
$('.toggle-legacy-mega').on('click', function(e) {
	if ( $('.mega-menu:not(.legacy-mega)').hasClass("is-active") ) {
		//nothing
	} else {
		$('body').toggleClass('no-scroll');
	}
	$('.toggle-mega:not(.toggle-legacy-mega)').removeClass("is-active");
	$('.mega-menu:not(.legacy-mega)').removeClass("is-active");
	$(this).toggleClass("is-active");
	$('.legacy-mega').toggleClass("is-active");
});
$('.toggle-support-mega').on('click', function(e) {
	if ( $('.mega-menu:not(.support-mega)').hasClass("is-active") ) {
		//nothing
	} else {
		$('body').toggleClass('no-scroll');
	}
	$('.toggle-mega:not(.toggle-support-mega)').removeClass("is-active");
	$('.mega-menu:not(.support-mega)').removeClass("is-active");
	$(this).toggleClass("is-active");
	$('.support-mega').toggleClass("is-active");
});
$('.toggle-programs-mega').on('click', function(e) {
	if ( $('.mega-menu:not(.programs-mega)').hasClass("is-active") ) {
		//nothing
	} else {
		$('body').toggleClass('no-scroll');
	}
	$('.toggle-mega:not(.toggle-programs-mega)').removeClass("is-active");
	$('.mega-menu:not(.programs-mega)').removeClass("is-active");
	$(this).toggleClass("is-active");
	$('.programs-mega').toggleClass("is-active");
});
$('.toggle-events-mega').on('click', function(e) {
	if ( $('.mega-menu:not(.events-mega)').hasClass("is-active") ) {
		//nothing
	} else {
		$('body').toggleClass('no-scroll');
	}
	$('.toggle-mega:not(.toggle-events-mega)').removeClass("is-active");
	$('.mega-menu:not(.events-mega)').removeClass("is-active");
	$(this).toggleClass("is-active");
	$('.events-mega').toggleClass("is-active");
});
$('.toggle-search-mega').on('click', function(e) {
	if ( $('.mega-menu:not(.search-mega)').hasClass("is-active") ) {
		//nothing
	} else {
		$('body').toggleClass('no-scroll');
	}
	$('.toggle-mega:not(.toggle-search-mega)').removeClass("is-active");
	$('.mega-menu:not(.search-mega)').removeClass("is-active");
	$(this).toggleClass("is-active");
	$('.search-mega').toggleClass("is-active");
});
$('.toggle-menu').on('click', function(e) {
	$('body').toggleClass('no-scroll');
	$('.mega-menu').removeClass("is-active");
	$(this).toggleClass("is-active");
	$('.mobile-menu').toggleClass("is-active");
});
/*----------------------------------------------------------------*\
	MOBILE MENU TOGGLES
\*----------------------------------------------------------------*/
$('.mobile-menu .menu-item-has-children > a').on('click', function(e) {
	e.preventDefault();
	$(this).parent('.menu-item-has-children').toggleClass("is-active");
	$(this).siblings('.sub-menu').toggleClass("is-active");
});
$('html').click(function (event) {
	if ($(event.target).closest('.mobile-menu, .toggle-menu').length === 0) {
		$('.toggle-menu').removeClass("is-active");
		$('.mobile-menu').removeClass("is-active");
		$('html').removeClass("no-scroll");
	}
});
/*----------------------------------------------------------------*\
	LAZYLOAD IFRAME
\*----------------------------------------------------------------*/
$(function () {
	$('iframe').addClass('lazyload');
});
/*----------------------------------------------------------------*\
	STICKY MENU - add class when user scroll up beyond 100vh
\*----------------------------------------------------------------*/
var lastScrollTop = 0;
var $win = $(window);
var winH = $win.height();  
var winH2 = $win.height() / 2; 
$(window).scroll(function(event){
	var scroll = $(window).scrollTop();
	var st = $(this).scrollTop();
	if ($(this).scrollTop() > winH ) {
		$(".primary-navigation").addClass("is-sticky");
		if (st > lastScrollTop){
			$(".primary-navigation").addClass("is-scrolling-down");
			$(".primary-navigation").removeClass("is-scrolling-up");
		} else {
			$(".primary-navigation").addClass("is-scrolling-up");
			$(".primary-navigation").removeClass("is-scrolling-down");
		}
		lastScrollTop = st;
	} else if ($(this).scrollTop() > winH2 ) {
		$(".primary-navigation").addClass("is-pre-sticky");
	} else {
		$(".primary-navigation").removeClass("is-sticky");
		$(".primary-navigation").removeClass("is-pre-sticky");
	}
}).on("resize", function(){ // If the user resizes the window
	winH = $(this).height(); // you'll need the new height value
});
/*----------------------------------------------------------------*\
	PARALLAX
\*----------------------------------------------------------------*/
var rellax = new Rellax('.rellax');
/*----------------------------------------------------------------*\
	ENTRANCE ANIMATIONS
\*----------------------------------------------------------------*/
emergence.init({
	elemCushion: 0.4,
	offsetTop: -100,	
});