<?php 
/*----------------------------------------------------------------*\

	DEFAULT POST ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="blog-head post-head has-nothing">
	<div>
		<div>
			<?php if ( strlen(get_field('post_title', 'options')) > 29 ) : ?>
				<h1 class="is-long"><?php the_field('post_title', 'options'); ?></h1>
			<?php else : ?>
				<h1 class="is-standard"><?php the_field('post_title', 'options'); ?></h1>
			<?php endif; ?>
			<?php if ( get_field('post_description', 'option') ) : ?>
				<?php the_field('post_description', 'option'); ?>
			<?php endif; ?>
			<ul class="categories">
				<?php 
					$category = get_queried_object();
					$args = array(
						'hide_empty'         => 1,
						'orderby'            => 'name',
						'order'              => 'ASC',
						'show_count'         => 0,
						'use_desc_for_title' => 0,
						'title_li'           => 0,
						'separator'					 => '',
						'current_category' 	 => $category->term_id,
						'show_option_all'		 => 'All News',
					);
				?>
				<?php wp_list_categories($args); ?>
			</ul>
		</div>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="post-grid is-extra-wide">
				<?php	while ( have_posts() ) : the_post(); ?>
					<div class="post-card card">
						<?php if ( get_field('preview_image') ) : $image = get_field('preview_image');  ?>
							<figure>
								<div class="label is-black">
									<?php $categories = get_the_category(); ?>
									<?php echo $categories[0]->name; ?>
								</div>
								<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							</figure>
						<?php endif; ?>
						<h3><?php the_title(); ?></h3>
						<hr class="is-red">
						<p class="subheadline"><?php echo get_the_date( 'F Y' ); ?></p>
						<?php if ( get_field('preview_description') ) : ?>
							<p><?php the_field('preview_description'); ?></p>
						<?php endif; ?>
						<div class="buttons">
							<a class="button is-blue" href="<?php the_permalink(); ?>">
								Read More
							</a>
						</div>
					</div>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>