<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php 
	if ( get_field('featured_image') ) :
		$headerClass = 'has-image';
	else :
		$headerClass = 'has-nothing-narrow';
	endif; 
?>
<?php //TITLE SIZE CLASS BASED ON LENGTH
	$words = explode(' ', get_the_title());
	$longestWordLength = 0;
	$longestWord = '';
	foreach ($words as $word) {
		if (strlen($word) > $longestWordLength) {
			$longestWordLength = strlen($word);
			$longestWord = $word;
		}
	}
	if ( strlen(get_the_title()) > 29 ) :
		$sizeClass = 'is-long';
	elseif ( strlen($longestWord) > 10 ) :
		$sizeClass = 'is-long';
	else : 
		$sizeClass = 'is-standard';
	endif;
?>

<header class="post-head <?php echo $headerClass; ?>">
	<div>
		<div>
			<h1 class="<?php echo $sizeClass; ?>"><?php the_title(); ?></h1>
			<?php if ( get_field('starting_year') ) : ?>
				<p class="subheadline"><?php the_field('starting_statement'); ?> <?php the_field('starting_year'); ?></p>
			<?php endif; ?>
			<?php if( have_rows('current_members') ): ?>
				<div class="artists">
					<?php while( have_rows('current_members') ) : the_row(); ?>
						<div class="artist">
							<?php $image = get_sub_field('headshot'); ?>
							<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							<p><?php the_sub_field('name'); ?></p>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if ( get_field('featured_image') ) : ?>
			<?php $image = get_field('featured_image'); ?>
			<figure>
				<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</figure>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<?php if( have_rows('key_stats') ): ?>
		<section class="stats is-yellow">
			<div class="stat-grid is-extra-wide">
				<?php while( have_rows('key_stats') ) : the_row(); ?>
					<div class="stat">
						<p><b><?php the_sub_field('label'); ?></b></p>
						<p><?php the_sub_field('stat'); ?></p>
					</div>
				<?php endwhile; ?>
			</div>
		</section>
	<?php endif; ?>
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part( 'template-parts/sections/article' ); ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>