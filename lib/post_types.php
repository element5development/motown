<?php
/*----------------------------------------------------------------*\
	INITIALIZE POST TYPES
\*----------------------------------------------------------------*/
// Event
function create_event_cpt() {
	$labels = array(
		'name' => _x( 'Events', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Event', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Events', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Event', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Event Archives', 'textdomain' ),
		'attributes' => __( 'Event Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Event:', 'textdomain' ),
		'all_items' => __( 'All Events', 'textdomain' ),
		'add_new_item' => __( 'Add New Event', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Event', 'textdomain' ),
		'edit_item' => __( 'Edit Event', 'textdomain' ),
		'update_item' => __( 'Update Event', 'textdomain' ),
		'view_item' => __( 'View Event', 'textdomain' ),
		'view_items' => __( 'View Events', 'textdomain' ),
		'search_items' => __( 'Search Event', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Event', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Event', 'textdomain' ),
		'items_list' => __( 'Events list', 'textdomain' ),
		'items_list_navigation' => __( 'Events list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Events list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Event', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-calendar',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'event', $args );
}
add_action( 'init', 'create_event_cpt', 0 );
// POSTS PER PAGE
function change_events_per_page( $query ) {
	if ( $query->is_post_type_archive( 'event' ) && ! is_admin() && $query->is_main_query() ) {
		$query->set( 'posts_per_page', '5' );
	}
	return $query;
}
add_filter( 'pre_get_posts', 'change_events_per_page' );

// Program
function create_program_cpt() {
	$labels = array(
		'name' => _x( 'Programs', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Program', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Programs', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Program', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Program Archives', 'textdomain' ),
		'attributes' => __( 'Program Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Program:', 'textdomain' ),
		'all_items' => __( 'All Programs', 'textdomain' ),
		'add_new_item' => __( 'Add New Program', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Program', 'textdomain' ),
		'edit_item' => __( 'Edit Program', 'textdomain' ),
		'update_item' => __( 'Update Program', 'textdomain' ),
		'view_item' => __( 'View Program', 'textdomain' ),
		'view_items' => __( 'View Programs', 'textdomain' ),
		'search_items' => __( 'Search Program', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Program', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Program', 'textdomain' ),
		'items_list' => __( 'Programs list', 'textdomain' ),
		'items_list_navigation' => __( 'Programs list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Programs list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Program', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-flag',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'program', $args );
}
add_action( 'init', 'create_program_cpt', 0 );
// Artist
function create_artist_cpt() {
	$labels = array(
		'name' => _x( 'Artists', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Artist', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Artists', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Artist', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Artist Archives', 'textdomain' ),
		'attributes' => __( 'Artist Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Artist:', 'textdomain' ),
		'all_items' => __( 'All Artists', 'textdomain' ),
		'add_new_item' => __( 'Add New Artist', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Artist', 'textdomain' ),
		'edit_item' => __( 'Edit Artist', 'textdomain' ),
		'update_item' => __( 'Update Artist', 'textdomain' ),
		'view_item' => __( 'View Artist', 'textdomain' ),
		'view_items' => __( 'View Artists', 'textdomain' ),
		'search_items' => __( 'Search Artist', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Artist', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Artist', 'textdomain' ),
		'items_list' => __( 'Artists list', 'textdomain' ),
		'items_list_navigation' => __( 'Artists list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Artists list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Artist', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-audio',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'artist', $args );
}
add_action( 'init', 'create_artist_cpt', 0 );

// POSTS PER PAGE
function change_artists_per_page( $query ) {
	if ( $query->is_post_type_archive( 'artist' ) && ! is_admin() && $query->is_main_query() ) {
		$query->set( 'posts_per_page', '14' );
	}
	return $query;
}
add_filter( 'pre_get_posts', 'change_artists_per_page' );

/*----------------------------------------------------------------*\
  ARCHIVE SEO
\*----------------------------------------------------------------*/
add_filter( 'the_seo_framework_title_from_generation', function( $title, $args ) {
  /** 
   * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
   */
  if ( is_post_type_archive( 'artist' ) ) {
    $title = 'Artists | Motown Museum | Home of Hitsville U.S.A.';
  } elseif ( is_post_type_archive( 'event' ) ) {
    $title = 'Events | Motown Museum | Home of Hitsville U.S.A.';
  } elseif ( is_post_type_archive( 'program' ) ) {
    $title = 'Programs | Motown Museum | Home of Hitsville U.S.A.';
  }
  return $title;
}, 10, 2 );
add_filter( 'the_seo_framework_generated_description', function( $description, $args ) {
  /** 
   * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
   */
  if ( is_post_type_archive( 'artist' ) ) {
    $description = 'The list of Motown artists reads like a who’s who in music—Diana Ross & the Supremes, Smokey Robinson & the Miracles, Stevie Wonder, the Temptations, Marvin Gaye and so many more.';
  } elseif ( is_post_type_archive( 'event' ) ) {
    $description = 'From memorable galas and concert performances, to community celebrations and educational programs, Motown Museum hosts a range of special events throughout the year.';
  } elseif ( is_post_type_archive( 'program' ) ) {
    $description = 'Motown Museum’s uniquely curated community programs emphasize education and entrepreneurship, mentoring and exposure that nurtures and elevates tomorrow’s history makers.';
  }
  return $description;
}, 10, 2 );