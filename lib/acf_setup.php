<?php
/*----------------------------------------------------------------*\
	OPTION PAGES
\*----------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Archives',
		'menu_title'	=> 'Archives',
		'menu_slug' 	=> 'archives-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'menu_slug' 	=> 'footer-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Mega Menu',
		'menu_title'	=> 'Mega Menu',
		'menu_slug' 	=> 'menu-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Notification',
		'menu_title'	=> 'Notification',
		'menu_slug' 	=> 'global-notification',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
?>