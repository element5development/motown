<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'mobile_navigation' => __( 'Mobile Menu' ),
		'footer_navigation' => __( 'Footer Menu' ),
		'legal_navigation' => __( 'Legal Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );