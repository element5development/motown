<?php
/*----------------------------------------------------------------*\
	GAVITY FORM TAB INDEX FIX
\*----------------------------------------------------------------*/
add_filter("gform_tabindex", function () {
	return false;
});

/*----------------------------------------------------------------*\
	ERROR ANCHORS
\*----------------------------------------------------------------*/
add_filter( 'gform_confirmation_anchor', 'your_function_name' );

/*----------------------------------------------------------------*\
	ENABLE LABEL VISABILITY
\*----------------------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*----------------------------------------------------------------*\
	CHANGE SUBMIT INPUTS TO BUTTONS
\*----------------------------------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="button is-yellow"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );