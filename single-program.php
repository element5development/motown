<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php 
	if ( get_field('featured_image') ) :
		$headerClass = 'has-image';
	else :
		$headerClass = 'has-nothing-narrow';
	endif; 

	$image = get_field('featured_image'); 
	$dateOpen = get_field('registration_open_date');
	$dateValue = DateTime::createFromFormat('Ymd', $dateOpen);
	$dateSoon = $dateValue->modify('-30 day');
	$dateSoon = $dateSoon->format('Ymd');
	$dateClosed = get_field('registration_close_date');
	$dateToday = date('Ymd');
	if ( $dateToday > $dateOpen && $dateToday < $dateClosed ) :
		$labelClass = "is-green";
		$label = 'Open';
	elseif ( $dateToday < $dateOpen && $dateToday > $dateSoon ) :
		$labelClass = "is-yellow";
		$label = 'Coming Soon';
	elseif ( $dateToday > $dateOpen && $dateToday > $dateClosed ) :
		$labelClass = "is-red";
		$label = 'Closed';
	else :
		$labelClass = "is-transparent";
		$label = '';
	endif;		
?>
<?php //TITLE SIZE CLASS BASED ON LENGTH
	$words = explode(' ', get_the_title());
	$longestWordLength = 0;
	$longestWord = '';
	foreach ($words as $word) {
		if (strlen($word) > $longestWordLength) {
			$longestWordLength = strlen($word);
			$longestWord = $word;
		}
	}
	if ( strlen(get_the_title()) > 29 ) :
		$sizeClass = 'is-long';
	elseif ( strlen($longestWord) > 10 ) :
		$sizeClass = 'is-long';
	else : 
		$sizeClass = 'is-standard';
	endif;
?>

<header class="post-head <?php echo $headerClass; ?>">
	<div>
		<div>
			<div class="label <?php echo $labelClass; ?>">
				<?php 
					if ( $label != '' ) : 
						the_field('apply_or_register'); 
						echo ' '.$label; 
					endif;
				?>
			</div>
			<h1 class="<?php echo $sizeClass; ?>"><?php the_title(); ?></h1>
			<?php if ( get_field('subheader') ) : ?>
				<p class="subheadline"><?php the_field('subheader'); ?></p>
			<?php endif; ?>
			<?php if( get_field('application_link') || get_field('application_pdf') ): ?>
				<div class="buttons">
					<?php if ( get_field('application_link') ) : ?>
						<a class="button is-yellow" href="<?php the_field('application_link'); ?>">
							<?php 
								if ( get_field('apply_or_register') == 'registration' ) :
									echo "Register Now";
								else :
									echo "Apply Now";
								endif;
							?>
						</a>
					<?php endif; ?>
					<?php if ( get_field('application_pdf') ) : $file = get_field('file'); ?>
						<a class="button is-transparent" href="<?php echo $file['url']; ?>">
							Download Application (PDF)
						</a>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if ( get_field('featured_image') ) : ?>
			<?php $image = get_field('featured_image'); ?>
			<figure>
				<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</figure>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<?php if( have_rows('key_stats') ): ?>
		<?php get_template_part( 'template-parts/sections/key-stats' ); ?>
	<?php endif; ?>
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part( 'template-parts/sections/article' ); ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>