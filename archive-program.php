<?php 
/*----------------------------------------------------------------*\

	PROGRAM ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head archive-head has-gradient <?php the_field('programs_gradient', 'option'); ?>">
	<div>
		<div>
			<?php if ( strlen(get_field('programs_title', 'option')) > 29 ) : ?>
				<h1 class="is-long"><?php the_field('programs_title', 'option'); ?></h1>
			<?php else : ?>
				<h1 class="is-standard"><?php the_field('programs_title', 'option'); ?></h1>
			<?php endif; ?>
			<?php if ( get_field('programs_description', 'option') ) : ?>
				<p><?php the_field('programs_description', 'option'); ?></p>
			<?php endif; ?>
		</div>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="card-grid">
				<?php	$i = 0; while ( have_posts() ) : the_post(); $i++; ?>
					<div class="program-card card" <?php if ( !is_front_page() && !wp_is_mobile() ) : ?>data-emergence="hidden"<?php endif; ?>>
						<?php 
							$image = get_field('featured_image'); 
							$dateOpen = get_field('registration_open_date');
							$dateValue = DateTime::createFromFormat('Ymd', $dateOpen);
							$dateSoon = $dateValue->modify('-30 day');
							$dateSoon = $dateSoon->format('Ymd');
							$dateClosed = get_field('registration_close_date');
							$dateToday = date('Ymd');
							if ( $dateToday > $dateOpen && $dateToday < $dateClosed ) :
								$labelClass = "is-green";
								$label = 'Open';
							elseif ( $dateToday < $dateOpen && $dateToday > $dateSoon ) :
								$labelClass = "is-yellow";
								$label = 'Coming Soon';
							elseif ( $dateToday > $dateOpen && $dateToday > $dateClosed ) :
								$labelClass = "is-red";
								$label = 'Closed';
							else :
								$labelClass = "is-transparent";
								$label = '';
							endif;
						?>
						<?php if ( get_field('featured_image') ) : ?>
							<figure>
								<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							</figure>
						<?php endif; ?>
						<div class="label <?php echo $labelClass; ?>">
							<?php 
								if ( $label != '' ) : 
									the_field('apply_or_register'); 
									echo ' '.$label; 
								endif;
							?>
						</div>
						<h3><?php the_title(); ?></h3>
						<hr class="is-purple">
						<?php if ( get_field('subheader') ) : ?>
							<p class="subheadline"><?php the_field('subheader') ?></p>
						<?php endif; ?>
						<?php if ( get_field('short_description') ) : ?>
							<p><?php the_field('short_description'); ?></p>
						<?php endif; ?>
						<div class="buttons">
							<a class="button is-blue" href="<?php the_permalink(); ?>">
								Learn More
							</a>
						</div>
					</div>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>