<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>
<section class="newsletter has-gradient <?php the_field('newsletter_gradient','option'); ?> is-full-width">
	<div class="is-standard">
		<div><h2>Join Our Newsletter</h2></div>
		<div><?php echo do_shortcode('[gravityform id="5" title="false" description="false"]'); ?></div>
	</div>
</section>

<footer class="post-footer">
	<div>
		<div class="contact">
			<?php if ( get_field('logo', 'option') ) : ?>
				<?php $image = get_field('logo', 'option'); ?>
				<img class="logo lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			<?php endif; ?>
			<?php if ( get_field('phone', 'option') ) : ?>
				<?php $phone = "tel:+1". preg_replace('/[^0-9]/', '',  get_field('phone', 'option') ); ?>
				<a href="<?php echo $phone; ?>"><?php the_field('phone', 'option'); ?></a>
			<?php endif; ?>
			<?php if ( get_field('address', 'option') ) : ?>
				<p><?php the_field('address', 'option'); ?></p>
			<?php endif; ?>
			<?php if( have_rows('social', 'option') ): ?>
				<ul>
					<?php while( have_rows('social', 'option') ) : the_row(); ?>
						<li>
							<a target="_blank" href="<?php the_sub_field('url'); ?>">
								<?php $image = get_sub_field('platform'); ?>
								<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							</a>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
		<div class="nav">
			<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation' )); ?>
		</div>
		<div class="legal">
			<?php wp_nav_menu(array( 'theme_location' => 'legal_navigation' )); ?>
		</div>
		<div class="copyright">
			<p>© <?php echo date('Y'); ?> Motown Museum. All rights reserved. Any form of media used is property of Motown Museum and may not be reproduced without written consent.</p>
		</div>
	</div>
</footer>