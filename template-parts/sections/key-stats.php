<section class="stats <?php the_field('stat_border_color'); ?>">
	<div class="stat-grid is-extra-wide">
		<?php while( have_rows('key_stats') ) : the_row(); ?>
			<div class="stat">
				<p><b><?php the_sub_field('label'); ?></b></p>
				<p><?php the_sub_field('stat'); ?></p>
			</div>
		<?php endwhile; ?>
	</div>
</section>