<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<?php //HEADER FORMAT VARIABLES
	if ( get_field('layout') == 'has-gradient' ) : 
		$headerClass = get_field('gradient');
	elseif ( get_field('layout') == 'has-background-image' ) :
		$backgroundImage = get_field('background_image');
		$headerClass = 'lazyload blur-up';
	else :
		$headerClass = '';
	endif; 
?>
<?php //TITLE SIZE CLASS BASED ON LENGTH
	$words = explode(' ', get_the_title());
	$longestWordLength = 0;
	$longestWord = '';
	foreach ($words as $word) {
		if (strlen($word) > $longestWordLength) {
			$longestWordLength = strlen($word);
			$longestWord = $word;
		}
	}
	if ( strlen(get_the_title()) > 29 ) :
		$sizeClass = 'is-long';
	elseif ( strlen($longestWord) > 10 ) :
		$sizeClass = 'is-long';
	else : 
		$sizeClass = 'is-standard';
	endif;
?>
<?php //BACKGROUND IMAGE
	$image = get_sub_field('background'); 
?>


<header class="post-head <?php the_field('layout'); ?> <?php echo $headerClass; ?>"
	<?php if ( $backgroundImage ) : ?>
		data-expand="150" data-bgset="<?php echo $backgroundImage['sizes']['small']; ?> 350w, <?php echo $backgroundImage['sizes']['medium']; ?> 700w, <?php echo $backgroundImage['sizes']['large']; ?> 1000w, <?php echo $backgroundImage['sizes']['xlarge']; ?> 1200w" data-sizes="auto"
	<?php endif; ?>
>
	<div class="<?php the_field('title_width'); ?>">
		<div>
			<?php if ( get_field('preheadline') ) : ?>
				<p class="preheadline <?php the_field('preheadline_color'); ?>"><?php the_field('preheadline'); ?></p>
			<?php endif; ?>
			<h1 class="<?php echo $sizeClass; ?>"><?php the_title(); ?></h1>
			<?php if ( is_page('427') && have_rows('hours', 'options') ) : ?>
				<?php 
					$i = 1; 
					$day = date('N');
				?>
				<?php while( have_rows('hours', 'options') ) : the_row(); ?>
					<?php if ( $i == $day ) : ?>
						<p class="subheadline"><?php the_sub_field('hours'); ?></p>
					<?php endif; ?>
				<?php $i++; endwhile; ?>
			<?php elseif ( get_field('subheadline') ) : ?>
				<p class="subheadline"><?php the_field('subheadline'); ?></p>
			<?php endif; ?>
			<?php if ( get_field('description') ) : ?>
				<p><?php the_field('description'); ?></p>
			<?php endif; ?>
			<?php if( have_rows('buttons') ): ?>
				<div class="buttons">
					<?php while( have_rows('buttons') ) : the_row();?>
						<?php
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
							if ( get_sub_field('button') ) : 
						?>
							<a class="button <?php the_sub_field('color'); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
								<?php echo esc_html($link_title); ?>
							</a>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if ( get_field('layout') == 'has-image' ) : ?>
			<?php $image = get_field('content_image'); ?>
			<figure>
				<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</figure>
		<?php endif; ?>
	</div>
</header>