<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	custom map for the "still going on" outdoor exhibit

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="exhibit-still-going-on-mobile editor is-narrow">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-01.jpg" />
	<h4>Intro</h4>
	<p><strong>Mother, Mother / There’s too many of you crying / Brother, brother, brother / There’s far too many of you dying / You know we’ve got to find a way / To bring some lovin’ here today</strong></p>
	<p>The Still Going On exhibition is a walking tour that stretches from Woodward Avenue to the front steps of Hitsville U.S.A. on West Grand Boulevard. In celebration of the 50th anniversary of Marvin Gaye’s album What’s Going On, the installation features selected tracks paired with historical and contemporary photographs that prompt us to consider what has and hasn’t changed since Marvin first asked us “What’s Going On?”</p>
	<p>What’s Going On has lingered in American cultural memory as it continues to speak to the widespread social struggles that are still going on decades after its release in 1971. Enraged by the suffering he saw across the US, Marvin Gaye composed a musical collection that soothed a nation divided by a war abroad in Vietnam and racial tensions at home.</p>
	<p>Drawing upon the jazz, gospel, and Afro-Caribbean music that inspired him, Gaye collaboratively produced a groundbreaking album that confronted the racism, violence, imperialism, and poverty that plagued the nation. As a notable departure from his previous work, the lyrics of What’s Going On channeled Gaye’s personal frustrations during this divisive era. The masterpiece altered his career’s direction and amplified the position of social messaging in mainstream music.</p>
	<hr>
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-02.jpg" />
	<p style="margin: 0; font-size: 12px;">Left: AFP, Getty  |  Right: Chris Tuite / ImageSPACE / Sipa USA</p>
	<h4>Civil Rights & BLM Protests</h4>
	<p><strong>Don’t punish me with brutality / Talk to me, so you can see; Song: What’s Going On</strong></p>
	<p>Marvin Gaye crafted “What’s Going On” from lyrics started by Four Tops member Obie Benson after witnessing peaceful anti-Vietnam War protesters beaten by police in Berkeley, California. Like many Americans, Gaye was troubled by the constant violence and turmoil he had witnessed for most of his adult life. Adding his own touches to the song, Gaye addressed a nation processing the impacts of a prolonged war and the recent assassinations of leaders such as Martin Luther King Jr. (d. 1968) and Senator Robert F. Kennedy (d. 1968).</p>
	<p>The song prominently featured a choir composed of Gaye’s layered vocals urging a divided society to come together. Fifty years later, his pleas for the traumas felt by American families still reflect the realities and systemic problems that drive people across the nation to protest for change.</p>
	<hr>
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-03.jpg" />
	<p style="margin: 0; font-size: 12px;">Left: RBM Vintage Images / Alamy Stock Photo  |  Right: ESSAM AL-SUDANI / AFP / Getty Images</p>
	<h4>War & International Conflict</h4>
	<p><strong>War is hell, when will it end? / When will people start getting’ together again?; Song: What’s Happening Brother</strong></p>
	<p>Support for the Vietnam War started to wane in the 1960s. A growing body of citizens publicly opposed what they saw as an unnecessary invasion that, by its end, took the lives of 58,000 Americans and over 3 million Vietnamese civilians, soldiers, and freedom fighters. Marvin Gaye drew on conversations with his brother, who had recently served in Vietnam, to write “What’s Happening Brother.” The song delves into the experiences of veterans returning to a deteriorating society pulled apart by racism and political differences.</p>
	<p>By mentioning a variety of issues, including changing communities and a struggling economy, Gaye captured much of the anxiety of the Cold War era. In our modern society, frayed by a decades-long War on Terror in the Middle East, gun violence, and increasing attacks on communities of color, Marvin Gaye’s question “Are things really getting better like the newspaper said?” still applies.</p>	
	<hr>
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-04.jpg" />
	<p style="margin: 0; font-size: 12px;">Left: Universal History Archive / Getty Images  |  Right: fotog / Getty Images</p>
	<h4>Climate Change/Air Pollution</h4>
	<p><strong>Where did all the blue skies go? / Poison is the wind that blows; Song: Mercy, Mercy Me</strong></p>
	<p>After decades without environmental regulation, the public awoke in the 1960s to the devastating impacts of industrial growth. Factory smoke and car exhaust hung in the air, pesticides poisoned birds, litter piled up along roads, and toxic waste polluted waterways. Drawing upon the concept of ecology, Gaye laments the destruction humans inflict on our fellow beings and urges us to protect the earth in "Mercy, Mercy Me (The Ecology)."</p>
	<p>Following the first Earth Day on April 22, 1970, the public demanded change, bringing a wave of environmental regulations to the US. Nevertheless, society has continued to grow, and pollution has increased along with it. Scientists have repeatedly alerted the public to the dangers of human-induced climate change. As a new series of environmental crises threaten the future of Mother Earth, we must reflect upon the question, How much more abuse from man can she stand?</p>
	<hr>
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-05.jpg" />
	<p style="margin: 0; font-size: 12px;">Left: Getty Images, Bettmann (Photo by Marty Hanley)  |  Right: Getty Images, Boston Globe (Photo by Craig F. Walker)</p>
	<h4>Children’s Issues, Rights, & Activism</h4>
	<p><strong>Little children today / are really going to suffer tomorrow; Song: Save The Children</strong></p>
	<p>Marvin Gaye and his collaborators composed the album What’s Going On while their children played in the backyard. As fathers, they were painfully aware that their young children would inherit the world’s problems if their generation failed to confront the conditions that produce violence and injustice. In his earnestness, Gaye appealed to his adult listeners to find a way to save the babies.</p>
	<p>However, young people are often at the forefront of social movements working to reimagine the world around them. During the Civil Rights Era, children staged school walkouts to protest segregation, and college-aged activists founded the Student Nonviolent Coordinating Committee to oppose white supremacy. Today, students lead the nation in demanding action on gun violence and climate change through public demonstrations, such as the March for Our Lives and School Strike for Climate. </p>
	<hr>
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-06.jpg" />
	<p style="margin: 0; font-size: 12px;">Left: Wally McNamee / Getty Images  |  Right: Natasha Moustache / Getty Images</p>
	<h4>Respect One Another, Love</h4>
	<p><strong>And all He asks of us / Is we give each other love; Song: God Is Love</strong></p>
	<p>On the album What’s Going On, Marvin Gaye unleashed his frustrations with the nation’s failing systems. However, in songs such as “God Is Love” Gaye provided spaces for audiences to meditate upon his messages. Drawing on his Christian faith, he proposes people come together in love and peace to unite their communities. Gaye’s words reflect the efforts taking place in his Detroit community, which was rapidly organizing at the time to heal lingering wounds from the 1967 Rebellion. </p>
	<p>In the wake of a global pandemic, a time of immense unrest, and deepening community division, Americans today are still challenging the broken systems that impact their lives. Gaye’s suggestion endures as people rally around calls for peace and radical compassion with the hope of a brighter future for all people.</p>
	<hr>
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-07.jpg" />
	<p style="margin: 0; font-size: 12px;">Left: Stuart Lutz / Gado / Getty Images  |  Right: Alex Wong / Getty Images</p>
	<h4>Police Violence & Community Unrest</h4>
	<p><strong>Crime is increasing / Trigger happy policing / Panic is spreading; Song: Inner City Blues</strong></p>
	<p>In 1965, President Lyndon B. Johnson declared a national War on Crime, funneling federal money to local police departments. This funding provided military training and equipment to officers and greenlit increased surveillance of citizens perceived as ‘radical.’ Marvin Gaye’s “Inner City Blues (Makes Me Wanna Holler)” spoke to the nature of the inner-city experience, shaped by these national trends in law enforcement. By the 1970s, African American Detroiters were exhausted from mourning countless deaths of citizens at the hands of police officers and years of calling for reforms to no avail.</p>
	<p>Fifty years later, “Inner City Blues” still describes an American population traumatized by repeated incidents of police brutality. The song’s messages were startlingly relevant in the summer of 2020 after the deaths of George Floyd, Breonna Taylor, and Ahmaud Arbery, among too many others.</p>		
</section>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="exhibit-still-going-on is-extra-wide">
	<div class="outdoor-exhibit">
		<button class="marker-1" data-micromodal-trigger="modal-1"></button>
		<button class="marker-2" data-micromodal-trigger="modal-2"></button>
		<button class="marker-3" data-micromodal-trigger="modal-3"></button>
		<button class="marker-4" data-micromodal-trigger="modal-4"></button>
		<button class="marker-5" data-micromodal-trigger="modal-5"></button>
		<button class="marker-6" data-micromodal-trigger="modal-6"></button>
		<button class="marker-7" data-micromodal-trigger="modal-7"></button>
		<button class="marker-8" data-micromodal-trigger="modal-1"></button>
	</div>
</section>

<div class="modal micromodal-slide" id="modal-1" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true">
			<div class="actions">
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
				<button class="modal__next" data-micromodal-trigger="modal-2" data-micromodal-close></button>
			</div>
			<header class="modal__header">
				<h2>Intro</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-01.jpg" />
				<p><strong>Mother, Mother / There’s too many of you crying / Brother, brother, brother / There’s far too many of you dying / You know we’ve got to find a way / To bring some lovin’ here today</strong></p>
				<p>The Still Going On exhibition is a walking tour that stretches from Woodward Avenue to the front steps of Hitsville U.S.A. on West Grand Boulevard. In celebration of the 50th anniversary of Marvin Gaye’s album What’s Going On, the installation features selected tracks paired with historical and contemporary photographs that prompt us to consider what has and hasn’t changed since Marvin first asked us “What’s Going On?”</p>
				<p>What’s Going On has lingered in American cultural memory as it continues to speak to the widespread social struggles that are still going on decades after its release in 1971. Enraged by the suffering he saw across the US, Marvin Gaye composed a musical collection that soothed a nation divided by a war abroad in Vietnam and racial tensions at home.</p>
				<p>Drawing upon the jazz, gospel, and Afro-Caribbean music that inspired him, Gaye collaboratively produced a groundbreaking album that confronted the racism, violence, imperialism, and poverty that plagued the nation. As a notable departure from his previous work, the lyrics of What’s Going On channeled Gaye’s personal frustrations during this divisive era. The masterpiece altered his career’s direction and amplified the position of social messaging in mainstream music.</p>
			</main>
		</div>
	</div>
</div>
<div class="modal micromodal-slide" id="modal-2" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true">
			<div class="actions">
				<button class="modal__prev" data-micromodal-trigger="modal-1" data-micromodal-close></button>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
				<button class="modal__next" data-micromodal-trigger="modal-3" data-micromodal-close></button>
			</div>
			<header class="modal__header">
				<h2>Civil Rights & BLM Protests</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-02.jpg" />
				<p style="margin: 0; font-size: 12px;">Left: AFP, Getty  |  Right: Chris Tuite / ImageSPACE / Sipa USA</p>
				<p><strong>Don’t punish me with brutality / Talk to me, so you can see; Song: What’s Going On</strong></p>
				<p>Marvin Gaye crafted “What’s Going On” from lyrics started by Four Tops member Obie Benson after witnessing peaceful anti-Vietnam War protesters beaten by police in Berkeley, California. Like many Americans, Gaye was troubled by the constant violence and turmoil he had witnessed for most of his adult life. Adding his own touches to the song, Gaye addressed a nation processing the impacts of a prolonged war and the recent assassinations of leaders such as Martin Luther King Jr. (d. 1968) and Senator Robert F. Kennedy (d. 1968).</p>
				<p>The song prominently featured a choir composed of Gaye’s layered vocals urging a divided society to come together. Fifty years later, his pleas for the traumas felt by American families still reflect the realities and systemic problems that drive people across the nation to protest for change.</p>
			</main>
		</div>
	</div>
</div>
<div class="modal micromodal-slide" id="modal-3" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true">
			<div class="actions">
				<button class="modal__prev" data-micromodal-trigger="modal-2" data-micromodal-close></button>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
				<button class="modal__next" data-micromodal-trigger="modal-4" data-micromodal-close></button>
			</div>
			<header class="modal__header">
				<h2>War & International Conflict</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-03.jpg" />
				<p style="margin: 0; font-size: 12px;">Left: RBM Vintage Images / Alamy Stock Photo  |  Right: ESSAM AL-SUDANI / AFP / Getty Images</p>
				<p><strong>War is hell, when will it end? / When will people start getting’ together again?; Song: What’s Happening Brother</strong></p>
				<p>Support for the Vietnam War started to wane in the 1960s. A growing body of citizens publicly opposed what they saw as an unnecessary invasion that, by its end, took the lives of 58,000 Americans and over 3 million Vietnamese civilians, soldiers, and freedom fighters. Marvin Gaye drew on conversations with his brother, who had recently served in Vietnam, to write “What’s Happening Brother.” The song delves into the experiences of veterans returning to a deteriorating society pulled apart by racism and political differences.</p>
				<p>By mentioning a variety of issues, including changing communities and a struggling economy, Gaye captured much of the anxiety of the Cold War era. In our modern society, frayed by a decades-long War on Terror in the Middle East, gun violence, and increasing attacks on communities of color, Marvin Gaye’s question “Are things really getting better like the newspaper said?” still applies.</p>
			</main>
		</div>
	</div>
</div>
<div class="modal micromodal-slide" id="modal-4" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true">
			<div class="actions">
				<button class="modal__prev" data-micromodal-trigger="modal-3" data-micromodal-close></button>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
				<button class="modal__next" data-micromodal-trigger="modal-5" data-micromodal-close></button>
			</div>
			<header class="modal__header">
				<h2>Climate Change/Air Pollution</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-04.jpg" />
				<p style="margin: 0; font-size: 12px;">Left: Universal History Archive / Getty Images  |  Right: fotog / Getty Images</p>
				<p><strong>Where did all the blue skies go? / Poison is the wind that blows; Song: Mercy, Mercy Me</strong></p>
				<p>After decades without environmental regulation, the public awoke in the 1960s to the devastating impacts of industrial growth. Factory smoke and car exhaust hung in the air, pesticides poisoned birds, litter piled up along roads, and toxic waste polluted waterways. Drawing upon the concept of ecology, Gaye laments the destruction humans inflict on our fellow beings and urges us to protect the earth in "Mercy, Mercy Me (The Ecology)."</p>
				<p>Following the first Earth Day on April 22, 1970, the public demanded change, bringing a wave of environmental regulations to the US. Nevertheless, society has continued to grow, and pollution has increased along with it. Scientists have repeatedly alerted the public to the dangers of human-induced climate change. As a new series of environmental crises threaten the future of Mother Earth, we must reflect upon the question, How much more abuse from man can she stand?</p>
			</main>
		</div>
	</div>
</div>
<div class="modal micromodal-slide" id="modal-5" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true">
			<div class="actions">
				<button class="modal__prev" data-micromodal-trigger="modal-4" data-micromodal-close></button>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
				<button class="modal__next" data-micromodal-trigger="modal-6" data-micromodal-close></button>
			</div>
			<header class="modal__header">
				<h2>Children’s Issues, Rights, & Activism</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-05.jpg" />
				<p style="margin: 0; font-size: 12px;">Left: Getty Images, Bettmann (Photo by Marty Hanley)  |  Right: Getty Images, Boston Globe (Photo by Craig F. Walker)</p>
				<p><strong>Little children today / are really going to suffer tomorrow; Song: Save The Children</strong></p>
				<p>Marvin Gaye and his collaborators composed the album What’s Going On while their children played in the backyard. As fathers, they were painfully aware that their young children would inherit the world’s problems if their generation failed to confront the conditions that produce violence and injustice. In his earnestness, Gaye appealed to his adult listeners to find a way to save the babies.</p>
				<p>However, young people are often at the forefront of social movements working to reimagine the world around them. During the Civil Rights Era, children staged school walkouts to protest segregation, and college-aged activists founded the Student Nonviolent Coordinating Committee to oppose white supremacy. Today, students lead the nation in demanding action on gun violence and climate change through public demonstrations, such as the March for Our Lives and School Strike for Climate. </p>
			</main>
		</div>
	</div>
</div>
<div class="modal micromodal-slide" id="modal-6" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true">
			<div class="actions">
				<button class="modal__prev" data-micromodal-trigger="modal-5" data-micromodal-close></button>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
				<button class="modal__next" data-micromodal-trigger="modal-7" data-micromodal-close></button>
			</div>
			<header class="modal__header">
				<h2>Respect One Another, Love</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-06.jpg" />
				<p style="margin: 0; font-size: 12px;">Left: Wally McNamee / Getty Images  |  Right: Natasha Moustache / Getty Images</p>
				<p><strong>And all He asks of us / Is we give each other love; Song: God Is Love</strong></p>
				<p>On the album What’s Going On, Marvin Gaye unleashed his frustrations with the nation’s failing systems. However, in songs such as “God Is Love” Gaye provided spaces for audiences to meditate upon his messages. Drawing on his Christian faith, he proposes people come together in love and peace to unite their communities. Gaye’s words reflect the efforts taking place in his Detroit community, which was rapidly organizing at the time to heal lingering wounds from the 1967 Rebellion. </p>
				<p>In the wake of a global pandemic, a time of immense unrest, and deepening community division, Americans today are still challenging the broken systems that impact their lives. Gaye’s suggestion endures as people rally around calls for peace and radical compassion with the hope of a brighter future for all people.</p>
			</main>
		</div>
	</div>
</div>
<div class="modal micromodal-slide" id="modal-7" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true">
			<div class="actions">
				<button class="modal__prev" data-micromodal-trigger="modal-6" data-micromodal-close></button>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
			</div>
			<header class="modal__header">
				<h2>Police Violence & Community Unrest</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/exhibit/still-going-on-step-07.jpg" />
				<p style="margin: 0; font-size: 12px;">Left: Stuart Lutz / Gado / Getty Images  |  Right: Alex Wong / Getty Images</p>
				<p><strong>Crime is increasing / Trigger happy policing / Panic is spreading; Song: Inner City Blues</strong></p>
				<p>In 1965, President Lyndon B. Johnson declared a national War on Crime, funneling federal money to local police departments. This funding provided military training and equipment to officers and greenlit increased surveillance of citizens perceived as ‘radical.’ Marvin Gaye’s “Inner City Blues (Makes Me Wanna Holler)” spoke to the nature of the inner-city experience, shaped by these national trends in law enforcement. By the 1970s, African American Detroiters were exhausted from mourning countless deaths of citizens at the hands of police officers and years of calling for reforms to no avail.</p>
				<p>Fifty years later, “Inner City Blues” still describes an American population traumatized by repeated incidents of police brutality. The song’s messages were startlingly relevant in the summer of 2020 after the deaths of George Floyd, Breonna Taylor, and Ahmaud Arbery, among too many others.</p>
			</main>
		</div>
	</div>
</div>