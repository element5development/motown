<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 featured posts

\*----------------------------------------------------------------*/
?>

<?php 
	$featured_posts = get_sub_field('featured_posts');
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="featured-posts is-extra-wide">
	<?php if ( get_sub_field('featured_posts_headline') ) : ?>
		<h2><?php the_sub_field('featured_posts_headline'); ?></h2>
	<?php endif; ?>
	<div class="post-grid">
		<?php foreach( $featured_posts as $featured_post ): ?>
			<div class="post-card card">
				<?php if ( get_field('preview_image', $featured_post->ID) ) : $image = get_field('preview_image', $featured_post->ID);  ?>
					<figure>
						<div class="label is-black">
							<?php $categories = get_the_category(); ?>
							<?php echo $categories[0]->name; ?>
						</div>
						<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					</figure>
				<?php endif; ?>
				<h4><?php echo get_the_title($featured_post->ID); ?></h4>
				<hr class="is-red">
				<p class="subheadline"><?php echo get_the_date( 'F Y', $featured_post->ID ); ?></p>
				<?php if ( get_field('preview_description', $featured_post->ID) ) : ?>
					<p><?php the_field('preview_description', $featured_post->ID); ?></p>
				<?php endif; ?>
				<div class="buttons">
					<a class="button is-blue" href="<?php the_permalink($featured_post->ID); ?>">
						Read More
					</a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>