<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying a series of links

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="link-list <?php the_sub_field('width'); ?>">
	<?php if ( get_sub_field('section_headline') ) : ?>
		<h2><?php the_sub_field('section_headline'); ?></h2>
	<?php endif; ?>
	<div class="links">
		<?php while( have_rows('links') ) : the_row(); ?>
			<?php
				$link = get_sub_field('link');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self'; 
				$image = get_sub_field('icon');
			?>
			<a class="button is-transparent" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				<?php echo esc_html( $link_title ); ?>
			</a>
		<?php endwhile; ?>
	</div>
</section>