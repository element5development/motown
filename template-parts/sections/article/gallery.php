<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying gallery of images

\*----------------------------------------------------------------*/
?>

<?php //GALLERY
	$images = get_sub_field('slider');
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="gallery <?php the_sub_field('width'); ?> <?php the_sub_field('format'); ?>">
	<?php while( have_rows('gallery') ) : the_row(); ?>
		<figure class="<?php the_sub_field('width'); ?>">
			<?php $image = get_sub_field('image'); ?>
			<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
		</figure>
	<?php endwhile; ?>
	<?php if ( $images ) : ?>
		<?php foreach( $images as $image ): ?>
			<figure>
				<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</figure>
		<?php endforeach; ?>
	<?php endif; ?>
</section>
