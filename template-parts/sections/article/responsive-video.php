<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying video embed within a responsive container

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="responsive-video <?php the_sub_field('width'); ?>">
	<div <?php if ( !is_front_page() && !wp_is_mobile() ) : ?>data-emergence="hidden"<?php endif; ?>>
		<?php the_sub_field('content'); ?>
	</div>
</section>