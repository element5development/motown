<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of cards

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="card-grid">
	<?php $i = 0; while ( have_rows('cards') ) : the_row(); $i++; ?>
		<div class="card" <?php if ( !wp_is_mobile() ) : ?>data-emergence="hidden"<?php endif; ?>>
			<?php $image = get_sub_field('image'); ?>
			<?php if ( get_sub_field('image') ) : ?>
				<figure>
					<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				</figure>
			<?php endif; ?>
			<?php if ( get_sub_field('title') ) : ?>
				<h3><?php the_sub_field('title') ?></h3>
			<?php endif; ?>
			<hr class="<?php the_sub_field('underline_color'); ?>">
			<?php if ( get_sub_field('subheadline') ) : ?>
				<p class="subheadline"><?php the_sub_field('subheadline') ?></p>
			<?php endif; ?>
			<?php if ( get_sub_field('description') ) : ?>
				<p><?php the_sub_field('description'); ?></p>
			<?php endif; ?>
			<?php if ( get_sub_field('button') || get_sub_field('button_two') ) : ?>
				<div class="buttons">
					<?php
						$link = get_sub_field('button'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self'; 
						if ( get_sub_field('button') ) : 
					?>
						<a class="button is-yellow" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
					<?php
						$link = get_sub_field('button_two'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self'; 
						if ( get_sub_field('button_two') ) : 
					?>
						<a class="button is-blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	<?php endwhile; ?>
</section>