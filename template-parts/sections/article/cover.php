<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying content with background image

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="cover <?php the_sub_field('width'); ?> <?php the_sub_field('image_alignment'); ?> <?php if ( !is_front_page() && !wp_is_mobile() ) : ?>rellax<?php endif; ?>" data-rellax-speed="6" data-rellax-percentage="0.5">
	<div class="content <?php the_sub_field('background'); ?>">
		<?php if ( get_sub_field('background') == 'black' ) : ?>
			<hr>
		<?php endif; ?>
		<?php the_sub_field('content'); ?>
	</div>
	<div class="image">
		<?php $img = get_sub_field('img'); ?>
		<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $img['sizes']['placeholder']; ?>" data-src="<?php echo $img['sizes']['large']; ?>" data-srcset="<?php echo $img['sizes']['small']; ?> 300w, <?php echo $img['sizes']['medium']; ?> 700w, <?php echo $img['sizes']['large']; ?> 1000w, <?php echo $img['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $img['alt']; ?>">
	</div>
</section>
