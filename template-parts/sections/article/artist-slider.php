<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying artists as a slider

\*----------------------------------------------------------------*/
?>

<?php //GALLERY
	$featured_posts = get_sub_field('artists');
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="artist-slider is-full-width">
	<?php foreach( $featured_posts as $featured_post ): ?>
		<div class="artist-card card">
			<?php if ( get_field('featured_image', $featured_post->ID) ) : $image = get_field('featured_image', $featured_post->ID); ?>
				<figure>
					<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				</figure>
			<?php endif; ?>
			<h4><?php echo get_the_title($featured_post->ID); ?></h4>
			<!-- 
				<hr class="is-blue">
				<?php if ( get_field('starting_year', $featured_post->ID) ) :  ?>
					<p>Starting in <?php the_field('starting_year', $featured_post->ID); ?></p>
				<?php endif; ?> 
			-->
			<a href="<?php the_permalink($featured_post->ID); ?>" class="button is-blue">Discover More</a>
		</div>
	<?php endforeach; ?>
</section>