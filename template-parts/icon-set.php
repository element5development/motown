<?php 
/*----------------------------------------------------------------*\

	SVG ICONS
	List of all the svg icons used throughout the site.
	Allows for easy access without repeat code.

\*----------------------------------------------------------------*/
?>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none">
	<symbol id="email" viewBox="0 0 64 64">
		<path data-name="layer2" fill="none" stroke-miterlimit="10" stroke-width="2" d="M2 12l30 27.4L62 12" stroke-linejoin="round" stroke-linecap="round"/>
		<path data-name="layer1" fill="none" stroke-miterlimit="10" stroke-width="2" d="M2 12h60v40H2z" stroke-linejoin="round" stroke-linecap="round"/>
	</symbol>
	<symbol id="phone" viewBox="0 0 64 64">
		<path data-name="layer1" d="M58.9 47l-10.4-6.8a4.8 4.8 0 0 0-6.5 1.3c-2.4 2.9-5.3 7.7-16.2-3.2S19.6 24.4 22.5 22a4.8 4.8 0 0 0 1.3-6.5L17 5.1c-.9-1.3-2.1-3.4-4.9-3S2 6.6 2 15.6s7.1 20 16.8 29.7S39.5 62 48.4 62s13.2-8 13.5-10-1.7-4.1-3-5z" fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"/>
	</symbol>
	<symbol id="search" viewBox="0 0 32 32">
		<g fill-rule="nonzero">
			<path d="M30.414 27.586L24.2 21.369a14.065 14.065 0 01-2.831 2.831l6.217 6.217a2 2 0 001.389.561c1.098 0 2-.902 2-2a2 2 0 00-.561-1.389v-.003zM13 25C6.417 25 1 19.583 1 13S6.417 1 13 1s12 5.417 12 12c-.007 6.58-5.42 11.993-12 12zm0-22C7.514 3 3 7.514 3 13s4.514 10 10 10 10-4.514 10-10c-.006-5.484-4.516-9.994-10-10z"/>
		</g>
	</symbol>
	<symbol id="cart" viewBox="0 0 32 32">
		<g class="nc-icon-wrapper">
			<path d="M2.678 16l3.348 14.229c.107.452.51.771.974.771h18c.464 0 .867-.319.974-.771L29.322 16H2.678z"/>
			<path data-color="color-2" d="M31 10h-5.465l-5.703-8.555a1 1 0 00-1.664 1.11L23.132 10H8.868l4.964-7.445a1.001 1.001 0 00-1.664-1.11L6.465 10H1a1 1 0 00-1 1v2a1 1 0 001 1h30a1 1 0 001-1v-2a1 1 0 00-1-1z"/>
		</g>
	</symbol>
	<symbol id="calendar" viewBox="0 0 32 32">
		<g class="nc-icon-wrapper">
			<path d="M29 4h-5V1a1 1 0 00-2 0v3H10V1a1 1 0 00-2 0v3H3a2 2 0 00-2 2v23a2 2 0 002 2h26a2 2 0 002-2V6a2 2 0 00-2-2zm-1 25H4a1 1 0 01-1-1V10a1 1 0 011-1h24a1 1 0 011 1v18a1 1 0 01-1 1z"/>
			<path data-color="color-2" d="M5 20h6v5H5zM5 13h6v5H5zM13 20h6v5h-6zM13 13h6v5h-6zM21 13h6v5h-6z"/>
		</g>
	</symbol>
	<symbol id="location" viewBox="0 0 32 32">
		<path d="M16 0C10.1 0 4 4.5 4 12c0 7.1 10.8 18.2 11.3 18.7.2.2.4.3.7.3s.5-.1.7-.3C17.2 30.2 28 19.2 28 12c0-7.5-6.1-12-12-12zm0 16c-2.2 0-4-1.8-4-4s1.8-4 4-4 4 1.8 4 4-1.8 4-4 4z"/>
	</symbol>
	<symbol id="money" viewBox="0 0 32 32">
		<path d="M17.5 13.863V6.889a15.487 15.487 0 013.907.743l1.85.739 1.485-3.714-1.858-.743a18.87 18.87 0 00-5.384-1.04V0h-3v2.912c-4.118.439-8.059 2.7-8.059 7.111 0 4.687 4.276 6.143 8.059 7.172v7.311a15.7 15.7 0 01-5.165-1.152l-1.789-.895-1.788 3.578 1.788.895a19.4 19.4 0 006.954 1.579V32h3v-3.515c4.941-.45 8.059-3.141 8.059-7.123 0-4.844-4.35-6.462-8.059-7.499zm-7.059-3.84c0-1.974 2.275-2.787 4.059-3.068v6.075c-2.735-.816-4.059-1.546-4.059-3.007zM17.5 24.466v-6.427c2.732.847 4.059 1.663 4.059 3.323 0 2.192-2.32 2.889-4.059 3.104z"/>
	</symbol>
	<symbol id="menu" viewBox="0 0 24 24">
		<g class="nc-icon-wrapper">
			<path data-color="color-2" d="M23 10H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h22c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1z"/>
			<path d="M23 2H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h22c.6 0 1-.4 1-1V3c0-.6-.4-1-1-1zM23 18H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h22c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1z"/>
		</g>
	</symbol>
	<symbol id="close" viewBox="0 0 24 24">
		<path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
	</symbol>
	<symbol id="add" viewBox="0 0 24 24">
		<path d="M18 11h-5V6h-2v5H6v2h5v5h2v-5h5v-2z"/>
	</symbol>
	<symbol id="delete" viewBox="0 0 24 24">
		<path d="M6 11h12v2H6z"/>
	</symbol>
</svg>