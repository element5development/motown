<?php if ( get_field('global_notification', 'options') ) : ?>
	<div class="global-notification r-to-p">
		<div class="is-extra-wide">
			<?php if ( get_field('notification_link', 'options') ) : ?>
				<a href="<?php the_field('notification_link', 'options'); ?>"></a>
			<?php endif; ?>
			<p><?php the_field('global_notification', 'options'); ?></p>
			<button>
				<svg><use xlink:href="#close"></use></svg> 
			</button>
		</div>
	</div>
<?php endif; ?>