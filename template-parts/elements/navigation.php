<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>

<div class="primary-navigation">
	<nav>
		<button class="toggle-search-mega">
			<svg>
				<use xlink:href="#search"></use>
			</svg>
			<svg>
				<use xlink:href="#close"></use>
			</svg>
		</button>
		<?php if ( get_field('logo', 'option') ) : ?>
			<?php $image = get_field('logo', 'option'); ?>
			<a href="<?php echo get_home_url(); ?>">
				<img class="logo lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</a>
		<?php endif; ?>
		<ul>
			<li>
				<button class="toggle-visit-mega toggle-mega ">Visit</button>
			</li>
			<li>
				<button class="toggle-programs-mega toggle-mega ">Programs</button>
			</li>
			<li>
				<button class="toggle-events-mega toggle-mega">Events</button>
			</li>
			<li>
				<button class="toggle-legacy-mega toggle-mega">Legacy</button>
			</li>
			<li>
				<a target="_blank" href="<?php the_field('shop_url', 'options'); ?>" class="button"><svg><use xlink:href="#cart"></use></svg> Shop</a>
			</li>
			<li>
				<button class="toggle-support-mega toggle-mega">Support</button>
			</li>
			<li>
				<button class="toggle-search-mega toggle-mega is-icon"> 
					<svg><use xlink:href="#search"></use></svg> 
					<svg><use xlink:href="#close"></use></svg>
				</button>
			</li>
		</ul>
		<button class="toggle-menu">
			<svg>
				<use xlink:href="#menu"></use>
			</svg>
		</button>
	</nav>
	<div class="mega-menu visit-mega">
		<div>
			<div class="mega-menu-grid">
				<div class="primary-card card">
					<?php if( have_rows('hours', 'options') ): ?>
						<h3>Museum Hours</h3>
						<ul>
							<?php while( have_rows('hours', 'options') ) : the_row(); ?>
								<li><?php the_sub_field('hours'); ?></li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<?php if( have_rows('holidays', 'options') ): $count = count(get_field('holidays', 'options')); $i = 0; ?>
						<p class="holidays">We are closed on 
							<?php while( have_rows('holidays', 'options') ) : the_row(); $i++; ?>
							<?php if ( $i == $count ) : ?>and <?php endif; ?><?php the_sub_field('holidays'); ?><?php if ( $i != $count ) : ?>,<?php endif; ?>
							<?php endwhile; ?>
						</p>
					<?php endif; ?>
					<?php the_field('visit_menu', 'option'); ?>
				</div>
				<?php while( have_rows('visit_cards', 'options') ) : the_row(); ?>
					<?php //FORMAT VARIABLES
						if ( get_sub_field('gradient_or_image') == 'has-gradient' ) : 
							$headerClass = get_sub_field('gradient_or_image') . ' ' . get_sub_field('gradient');
						elseif ( get_sub_field('gradient_or_image') == 'has-background-image' ) :
							$backgroundImage = get_sub_field('background_image');
							$headerClass =  get_sub_field('gradient_or_image') . ' ' . 'lazyload blur-up';
						else :
							$headerClass = '';
						endif; 
					?>
					<div class="basic-card card <?php echo $headerClass; ?>">
						<?php if ( $backgroundImage && get_sub_field('gradient_or_image') == 'has-background-image' ) : ?>
							<div class="image <?php echo $headerClass; ?>" data-expand="150" data-bgset="<?php echo $backgroundImage['sizes']['small']; ?> 350w, <?php echo $backgroundImage['sizes']['medium']; ?> 700w, <?php echo $backgroundImage['sizes']['large']; ?> 1000w, <?php echo $backgroundImage['sizes']['xlarge']; ?> 1200w" data-sizes="auto"></div>
						<?php endif; ?>
						<?php if ( get_sub_field('preheader') && get_sub_field('gradient_or_image') == 'has-gradient' ) : ?>
							<p class="preheader"><?php the_sub_field('preheader'); ?></p>
						<?php endif; ?>
						<?php if ( get_sub_field('header') && get_sub_field('gradient_or_image') == 'has-gradient' ) : ?>
							<h3><?php the_sub_field('header'); ?></h3>
						<?php endif; ?>
						<?php if ( get_sub_field('description') && get_sub_field('gradient_or_image') == 'has-background-image' ) : ?>
							<p><?php the_sub_field('description'); ?></p>
						<?php endif; ?>
						<?php
							if ( get_sub_field('button') ) : 
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
							<a class="button <?php the_sub_field('color'); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
								<?php echo esc_html($link_title); ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<div class="mega-menu programs-mega">
		<div>
			<div class="mega-menu-grid">
				<div class="primary-card card">
					<?php the_field('programs_menu', 'option'); ?>
				</div>
				<?php if ( get_field('program_cards', 'option') ) : ?>
					<?php $featured_posts = get_field('program_cards', 'option'); ?>
					<?php foreach( $featured_posts as $post ): setup_postdata($post); ?>
						<div class="program-card card">
							<?php 
								$image = get_field('featured_image'); 
								$dateOpen = get_field('registration_open_date');
								$dateValue = DateTime::createFromFormat('Ymd', $dateOpen);
								$dateSoon = $dateValue->modify('-30 day');
								$dateSoon = $dateSoon->format('Ymd');
								$dateClosed = get_field('registration_close_date');
								$dateToday = date('Ymd');
								if ( $dateToday > $dateOpen && $dateToday < $dateClosed ) :
									$labelClass = "is-green";
									$label = 'Open';
								elseif ( $dateToday < $dateOpen && $dateToday > $dateSoon ) :
									$labelClass = "is-yellow";
									$label = 'Coming Soon';
								elseif ( $dateToday > $dateOpen && $dateToday > $dateClosed ) :
									$labelClass = "is-red";
									$label = 'Closed';
								else :
									$labelClass = "is-transparent";
									$label = '';
								endif;
							?>
							<?php if ( get_field('featured_image') ) : ?>
								<figure>
									<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								</figure>
							<?php endif; ?>
							<div class="label <?php echo $labelClass; ?>">
								<?php 
									if ( $label != '' ) : 
										the_field('apply_or_register'); 
										echo ' '.$label; 
									endif;
								?>
							</div>
							<h4><?php the_title(); ?></h4>
							<hr class="is-purple">
							<?php if ( get_field('subheader') ) : ?>
								<p class="subheadline"><?php the_field('subheader'); ?></p>
							<?php endif; ?>
							<?php if ( get_field('short_description') ) : ?>
								<p><?php echo substr(get_field('short_description'), 0, 240); ?>...</p>
							<?php endif; ?>
							<a href="<?php the_permalink(); ?>" class="button is-blue">Learn More</a>
						</div>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="mega-menu events-mega">
		<div>
			<div class="mega-menu-grid">
				<div class="primary-card card">
					<?php the_field('events_menu', 'option'); ?>
				</div>
				<?php while( have_rows('event_cards', 'options') ) : the_row(); ?>
					<?php if ( get_sub_field('event_or_page') == 'event' ) : ?>
						<?php $featured_events = get_sub_field('event'); ?>
						<?php foreach( $featured_events as $post ): ?>
							<?php setup_postdata($post); ?>
							<div class="event-card card">
								<?php if ( get_field('featured_image') ) : $image = get_field('featured_image'); ?>
									<figure>
										<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
									</figure>
								<?php endif; ?>
								<div>
									<?php if ( get_field('fundrasier') ) : ?>
										<div class="label is-purple">Fundraiser</div>
									<?php endif; ?>
									<h4><?php the_title(); ?></h4>
									<hr class="is-yellow">
									<?php if ( get_field('date') ) : ?>
										<p>
											<svg><use xlink:href="#calendar"></use></svg> 
											<?php the_field('date'); ?>
										</p>
									<?php endif; ?>
									<?php if ( get_field('location') ) : ?>
										<p>
											<svg><use xlink:href="#location"></use></svg> 
											<?php the_field('location'); ?>
										</p>
									<?php endif; ?>
									<?php if ( get_field('price') ) : ?>
										<p>
											<svg><use xlink:href="#money"></use></svg> 
											<?php the_field('price'); ?>
										</p>
									<?php endif; ?>
									<a href="<?php the_permalink(); ?>" class="button is-blue">Learn More</a>
								</div>
							</div>
						<?php endforeach; ?>
						<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<?php //FORMAT VARIABLES
							if ( get_sub_field('gradient_or_image') == 'has-gradient' ) : 
								$headerClass = get_sub_field('gradient_or_image') . ' ' . get_sub_field('gradient');
							elseif ( get_sub_field('gradient_or_image') == 'has-background-image' ) :
								$backgroundImage = get_sub_field('background_image');
								$headerClass =  get_sub_field('gradient_or_image') . ' ' . 'lazyload blur-up';
							else :
								$headerClass = '';
							endif; 
						?>
						<div class="basic-card card <?php echo $headerClass; ?>">
							<?php if ( $backgroundImage && get_sub_field('gradient_or_image') == 'has-background-image' ) : ?>
								<div class="image <?php echo $headerClass; ?>" data-expand="150" data-bgset="<?php echo $backgroundImage['sizes']['small']; ?> 350w, <?php echo $backgroundImage['sizes']['medium']; ?> 700w, <?php echo $backgroundImage['sizes']['large']; ?> 1000w, <?php echo $backgroundImage['sizes']['xlarge']; ?> 1200w" data-sizes="auto"></div>
							<?php endif; ?>
							<?php if ( get_sub_field('preheader') && get_sub_field('gradient_or_image') == 'has-gradient' ) : ?>
								<p class="preheader"><?php the_sub_field('preheader'); ?></p>
							<?php endif; ?>
							<?php if ( get_sub_field('header') && get_sub_field('gradient_or_image') == 'has-gradient' ) : ?>
								<h3><?php the_sub_field('header'); ?></h3>
							<?php endif; ?>
							<?php if ( get_sub_field('description') && get_sub_field('gradient_or_image') == 'has-background-image' ) : ?>
								<p><?php the_sub_field('description'); ?></p>
							<?php endif; ?>
							<?php
								if ( get_sub_field('button') ) : 
								$link = get_sub_field('button'); 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self'; 
							?>
								<a class="button <?php the_sub_field('color'); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
									<?php echo esc_html($link_title); ?>
								</a>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<div class="mega-menu legacy-mega">
		<div>
			<div class="mega-menu-grid">
				<div class="primary-card card">
					<?php the_field('legacy_menu', 'option'); ?>
				</div>
				<?php while( have_rows('legacy_cards', 'options') ) : the_row(); ?>
					<?php //FORMAT VARIABLES
						if ( get_sub_field('gradient_or_image') == 'has-gradient' ) : 
							$headerClass = get_sub_field('gradient_or_image') . ' ' . get_sub_field('gradient');
						elseif ( get_sub_field('gradient_or_image') == 'has-background-image' ) :
							$backgroundImage = get_sub_field('background_image');
							$headerClass =  get_sub_field('gradient_or_image') . ' ' . 'lazyload blur-up';
						else :
							$headerClass = '';
						endif; 
					?>
					<div class="basic-card card <?php echo $headerClass; ?>">
						<?php if ( $backgroundImage && get_sub_field('gradient_or_image') == 'has-background-image' ) : ?>
							<div class="image <?php echo $headerClass; ?>" data-expand="150" data-bgset="<?php echo $backgroundImage['sizes']['small']; ?> 350w, <?php echo $backgroundImage['sizes']['medium']; ?> 700w, <?php echo $backgroundImage['sizes']['large']; ?> 1000w, <?php echo $backgroundImage['sizes']['xlarge']; ?> 1200w" data-sizes="auto"></div>
						<?php endif; ?>
						<?php if ( get_sub_field('preheader') && get_sub_field('gradient_or_image') == 'has-gradient' ) : ?>
							<p class="preheader"><?php the_sub_field('preheader'); ?></p>
						<?php endif; ?>
						<?php if ( get_sub_field('header') && get_sub_field('gradient_or_image') == 'has-gradient' ) : ?>
							<h3><?php the_sub_field('header'); ?></h3>
						<?php endif; ?>
						<?php if ( get_sub_field('description') && get_sub_field('gradient_or_image') == 'has-background-image' ) : ?>
							<p><?php the_sub_field('description'); ?></p>
						<?php endif; ?>
						<?php
							if ( get_sub_field('button') ) : 
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
							<a class="button <?php the_sub_field('color'); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
								<?php echo esc_html($link_title); ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<div class="mega-menu support-mega">
		<div>
			<div class="mega-menu-grid">
				<div class="primary-card card">
					<?php the_field('support_menu', 'option'); ?>
				</div>
				<?php while( have_rows('support_cards', 'options') ) : the_row(); ?>
					<?php //FORMAT VARIABLES
						if ( get_sub_field('gradient_or_image') == 'has-gradient' ) : 
							$headerClass = get_sub_field('gradient_or_image') . ' ' . get_sub_field('gradient');
						elseif ( get_sub_field('gradient_or_image') == 'has-background-image' ) :
							$backgroundImage = get_sub_field('background_image');
							$headerClass =  get_sub_field('gradient_or_image') . ' ' . 'lazyload blur-up';
						else :
							$headerClass = '';
						endif; 
					?>
					<div class="basic-card card <?php echo $headerClass; ?>">
						<?php if ( $backgroundImage && get_sub_field('gradient_or_image') == 'has-background-image' ) : ?>
							<div class="image <?php echo $headerClass; ?>" data-expand="150" data-bgset="<?php echo $backgroundImage['sizes']['small']; ?> 350w, <?php echo $backgroundImage['sizes']['medium']; ?> 700w, <?php echo $backgroundImage['sizes']['large']; ?> 1000w, <?php echo $backgroundImage['sizes']['xlarge']; ?> 1200w" data-sizes="auto"></div>
						<?php endif; ?>
						<?php if ( get_sub_field('preheader') && get_sub_field('gradient_or_image') == 'has-gradient' ) : ?>
							<p class="preheader"><?php the_sub_field('preheader'); ?></p>
						<?php endif; ?>
						<?php if ( get_sub_field('header') && get_sub_field('gradient_or_image') == 'has-gradient' ) : ?>
							<h3><?php the_sub_field('header'); ?></h3>
						<?php endif; ?>
						<?php if ( get_sub_field('description') && get_sub_field('gradient_or_image') == 'has-background-image' ) : ?>
							<p><?php the_sub_field('description'); ?></p>
						<?php endif; ?>
						<?php
							if ( get_sub_field('button') ) : 
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
							<a class="button <?php the_sub_field('color'); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
								<?php echo esc_html($link_title); ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<div class="mega-menu search-mega">
		<div>
			<div class="search-grid">
				<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
					<div>
						<label for="s" class="screen-reader-text">Search for</label>
						<input type="search" id="s" name="s" placeholder="<?php echo esc_attr_x( '...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" />
					</div>
					<button type="submit">Search</button>
				</form>
			</div>
		</div>
	</div>
	<div class="mobile-menu">
		<button class="toggle-menu">
			<svg>
				<use xlink:href="#close"></use>
			</svg>
		</button>
		<?php wp_nav_menu(array( 'theme_location' => 'mobile_navigation' )); ?>
		<?php if( have_rows('hours', 'options') ): ?>
			<div class="hours">
				<h4>Museum Hours</h4>
				<ul>
					<?php while( have_rows('hours', 'options') ) : the_row(); ?>
						<li><?php the_sub_field('hours'); ?></li>
					<?php endwhile; ?>
				</ul>
			</div>
		<?php endif; ?>
		<a href="/contact" class="button">Contact Us</a>
	</div>
</div>
<?php get_template_part( 'template-parts/elements/notification' ); ?>