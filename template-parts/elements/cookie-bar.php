<?php 
/*----------------------------------------------------------------*\

	COOKIE USE NOTIFICATION BAR
	cookies used must be cleared via WPengine support

\*----------------------------------------------------------------*/
?>
<div class="cookie-useage-notification">
	<button> 
		<svg><use xlink:href="#close"></use></svg> 
	</button>
	<p>By using our website, you accept our <a href="<?php echo get_privacy_policy_url(); ?>">use of cookies</a>. We use cookies to provide you with a great and possibly personalized experience. </p>
</div>