<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php 
	if ( get_field('featured_image') ) :
		$headerClass = 'has-image';
	else :
		$headerClass = 'has-nothing-narrow';
	endif; 
?>
<?php //TITLE SIZE CLASS BASED ON LENGTH
	$words = explode(' ', get_the_title());
	$longestWordLength = 0;
	$longestWord = '';
	foreach ($words as $word) {
		if (strlen($word) > $longestWordLength) {
			$longestWordLength = strlen($word);
			$longestWord = $word;
		}
	}
	if ( strlen(get_the_title()) > 29 ) :
		$sizeClass = 'is-long';
	elseif ( strlen($longestWord) > 10 ) :
		$sizeClass = 'is-long';
	else : 
		$sizeClass = 'is-standard';
	endif;
?>

<header class="post-head <?php echo $headerClass; ?>">
	<div>
		<div>
			<?php if ( get_field('fundrasier') ) : ?>
				<div class="label is-purple">
					Fundraiser
				</div>
			<?php endif; ?>
			<h1 class="<?php echo $sizeClass; ?>"><?php the_title(); ?></h1>
			<?php if ( get_field('date') ) : ?>
				<p>
					<svg><use xlink:href="#calendar"></use></svg> 
					<?php the_field('date'); ?>
				</p>
			<?php endif; ?>
			<?php if ( get_field('location') ) : ?>
				<p>
					<svg><use xlink:href="#location"></use></svg> 
					<?php the_field('location'); ?>
				</p>
			<?php endif; ?>
			<?php if ( get_field('price') ) : ?>
				<p>
					<svg><use xlink:href="#money"></use></svg> 
					<?php the_field('price'); ?>
				</p>
			<?php endif; ?>
			<?php if( get_field('register_button') ): ?>
				<?php 
					$link = get_field('register_button');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<div class="buttons">
					<a class="button is-yellow" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				</div>
			<?php endif; ?>
		</div>
		<?php if ( get_field('featured_image') ) : ?>
			<?php $image = get_field('featured_image'); ?>
			<figure>
				<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</figure>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<?php if( have_rows('key_stats') ): ?>
		<?php get_template_part( 'template-parts/sections/key-stats' ); ?>
	<?php endif; ?>
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part( 'template-parts/sections/article' ); ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>