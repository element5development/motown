<?php 
/*----------------------------------------------------------------*\

	EVENT ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head archive-head has-gradient <?php the_field('events_gradient', 'option'); ?>">
	<div>
		<div>
			<?php if ( strlen(get_field('events_title', 'option')) > 29 ) : ?>
				<h1 class="is-long"><?php the_field('events_title', 'option'); ?></h1>
			<?php else : ?>
				<h1 class="is-standard"><?php the_field('events_title', 'option'); ?></h1>
			<?php endif; ?>
			<?php if ( get_field('events_description', 'option') ) : ?>
				<p><?php the_field('events_description', 'option'); ?></p>
			<?php endif; ?>
		</div>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="card-grid">
				<?php $i = 0; while ( have_posts() ) : the_post(); $i++; ?>
					<div class="event-card card" <?php if ( !is_front_page() && !wp_is_mobile() ) : ?>data-emergence="hidden"<?php endif; ?>>
						<?php if ( get_field('featured_image') ) : $image = get_field('featured_image'); ?>
							<figure>
								<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							</figure>
						<?php endif; ?>
						<div>
							<?php if ( get_field('fundrasier') ) : ?>
								<div class="label is-purple">Fundraiser</div>
							<?php endif; ?>
							<h3><?php the_title(); ?></h3>
							<hr class="is-yellow">
							<?php if ( get_field('date') ) : ?>
								<p>
									<svg><use xlink:href="#calendar"></use></svg> 
									<?php the_field('date'); ?>
								</p>
							<?php endif; ?>
							<?php if ( get_field('location') ) : ?>
								<p>
									<svg><use xlink:href="#location"></use></svg> 
									<?php the_field('location'); ?>
								</p>
							<?php endif; ?>
							<?php if ( get_field('price') ) : ?>
								<p>
									<svg><use xlink:href="#money"></use></svg> 
									<?php the_field('price'); ?>
								</p>
							<?php endif; ?>
							<a href="<?php the_permalink(); ?>" class="button is-blue">Learn More</a>
						</div>
					</div>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>