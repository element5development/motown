<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head has-nothing-narrow">
	<div>
		<div>
			<?php if ( strlen(get_field('artists_title', 'option')) > 29 ) : ?>
				<h1 class="is-long"><?php the_field('artists_title', 'option'); ?></h1>
			<?php else : ?>
				<h1 class="is-standard"><?php the_field('artists_title', 'option'); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</header>

<main id="main-content">
	<article>	
		<?php if ( get_field('artists_description', 'options') ) : ?>
			<section class="editor is-narrow">
				<?php the_field('artists_description', 'options'); ?>
			</section>
		<?php endif; ?>
		<?php if (have_posts()) : ?>
			<section class="artist-grid is-extra-wide">
				<?php	while ( have_posts() ) : the_post(); ?>
					<div class="artist-card card">
						<?php if ( get_field('featured_image') ) : $image = get_field('featured_image'); ?>
							<figure>
								<img class="lazyload blur-up" data-expand="150" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							</figure>
						<?php endif; ?>
						<h3><?php the_title(); ?></h3>
						<hr class="is-blue">
						<?php if ( get_field('starting_year') ) :  ?>
							<p><?php the_field('starting_statement'); ?> <?php the_field('starting_year'); ?></p>
						<?php endif; ?>
						<a href="<?php the_permalink(); ?>" class="button is-blue">Discover More</a>
					</div>
				<?php endwhile; ?>
				<div class="alumni artist-card card has-gradient b-to-r">
					<?php the_field('alumni', 'options'); ?>
				</div>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>