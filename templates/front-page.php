<?php 
/*----------------------------------------------------------------*\

	Template Name: Front Page

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="front-page-slider">
	<div class="slides">
		<?php while( have_rows('slider') ) : the_row(); ?>
			<?php 
				if ( get_sub_field('layout') == 'has-background-image' ) : 
					$headerClasses = get_sub_field('layout') . ' lazyload blur-up';
				else : 
					$headerClasses = get_sub_field('layout');
				endif; 
			?>
			<div class="home-slide <?php echo $headerClasses; ?>"
				<?php if ( get_sub_field('layout') == 'has-background-image' ) : $backgroundImage = get_sub_field('background_image');  ?>
					data-expand="150" data-bgset="<?php echo $backgroundImage['sizes']['small']; ?> 350w, <?php echo $backgroundImage['sizes']['medium']; ?> 700w, <?php echo $backgroundImage['sizes']['large']; ?> 1000w, <?php echo $backgroundImage['sizes']['xlarge']; ?> 1200w" data-sizes="auto"
				<?php endif; ?>
			>
				<div class="contents">
					<?php if ( get_sub_field('preheadline') ) : ?>
						<p class="preheadline <?php the_sub_field('color'); ?>"><?php the_sub_field('preheadline'); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field('headline') ) : ?>
						<h1><?php the_sub_field('headline'); ?></h1>
					<?php endif; ?>
					<?php if ( get_sub_field('subheadline') ) : ?>
						<p class="subheadline is-yellow"><?php the_sub_field('subheadline'); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field('description') ) : ?>
						<p><?php the_sub_field('description'); ?></p>
					<?php endif; ?>
					<?php if( have_rows('buttons') ): ?>
						<div class="buttons">
							<?php while( have_rows('buttons') ) : the_row();?>
								<?php
									$link = get_sub_field('button'); 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self'; 
									if ( get_sub_field('button') ) : 
								?>
									<a class="button <?php the_sub_field('color'); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
										<?php echo esc_html($link_title); ?>
									</a>
								<?php endif; ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
				<?php if ( get_sub_field('layout') == 'has-background-video' ) :  ?>
					<?php if ( !wp_is_mobile() ) : ?>
						<div class="videoWrapper">
							<iframe width="100%" height="100%" src="https://www.youtube-nocookie.com/embed/<?php the_sub_field('background_video') ?>?playlist=<?php the_sub_field('background_video') ?>&autoplay=1&controls=1&loop=1&mute=1" frameborder="0" allow="autoplay;" allowfullscreen></iframe>
						</div>
					<?php else : ?>
						<img src="https://img.youtube.com/vi/<?php the_sub_field('background_video') ?>/maxresdefault.jpg" />
					<?php endif; ?>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	</div>
	<div class="nav-slides">
		<?php while( have_rows('slider') ) : the_row(); ?>
			<div class="nav-slide">
				<?php if ( get_sub_field('selection_label') ) : ?>
					<p class="label"><?php the_sub_field('selection_label'); ?></p>
				<?php endif; ?>
				<?php if ( get_sub_field('Selection_Title') ) : ?>
					<p class="contents"><?php the_sub_field('Selection_Title'); ?></p>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	</div>
</header>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part( 'template-parts/sections/article' ); ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>