<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php //TITLE SIZE CLASS BASED ON LENGTH
	$words = explode(' ', get_the_title());
	$longestWordLength = 0;
	$longestWord = '';
	foreach ($words as $word) {
		if (strlen($word) > $longestWordLength) {
			$longestWordLength = strlen($word);
			$longestWord = $word;
		}
	}
	if ( strlen(get_the_title()) > 29 ) :
		$sizeClass = 'is-long';
	elseif ( strlen($longestWord) > 10 ) :
		$sizeClass = 'is-long';
	else : 
		$sizeClass = 'is-standard';
	endif;
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head has-nothing-narrow">
	<div>
		<div>
			<p class="preheadline is-transparent">Search Results for:</p>
			<h1 class="<?php echo $sizeClass; ?>"><?php echo get_search_query(); ?></h1>
			<div class="search-grid">
				<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
					<div>
						<label for="s" class="screen-reader-text">Not happy with results? Try another search</label>
						<input type="search" id="s" name="s" placeholder="<?php echo esc_attr_x( '...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" />
					</div>
					<button type="submit">Search</button>
				</form>
			</div>
		</div>
	</div>
</header>

<main id="main-content">
	<?php if (have_posts()) : ?>
		<article>
			<?php	while ( have_posts() ) : the_post(); ?>

			<?php //PROGRAM REGISTRATION
				if ( 'program' == get_post_type() ) : 
					if ( get_field('featured_image') ) :
						$headerClass = 'has-image';
					else :
						$headerClass = 'has-nothing-narrow';
					endif; 

					$image = get_field('featured_image'); 
					$dateOpen = get_field('registration_open_date');
					$dateValue = DateTime::createFromFormat('Ymd', $dateOpen);
					$dateSoon = $dateValue->modify('-30 day');
					$dateSoon = $dateSoon->format('Ymd');
					$dateClosed = get_field('registration_close_date');
					$dateToday = date('Ymd');
					if ( $dateToday > $dateOpen && $dateToday < $dateClosed ) :
						$labelClass = "is-green";
						$label = 'Registration Open';
					elseif ( $dateToday < $dateOpen && $dateToday > $dateSoon ) :
						$labelClass = "is-yellow";
						$label = 'Registration Coming Soon';
					elseif ( $dateToday > $dateOpen && $dateToday > $dateClosed ) :
						$labelClass = "is-red";
						$label = 'Registration Closed';
					else :
						$labelClass = "is-transparent";
						$label = '';
					endif;		
				endif;
			?>


				<a href="<?php the_permalink(); ?>">
					<article class="search-result is-narrow <?php echo get_post_type(); ?>">
						<?php if ( 'program' == get_post_type() ) : ?>
							<p class="preheadline <?php echo $labelClass; ?>"><?php echo $label; ?></p>
						<?php elseif ( 'event' == get_post_type() && get_field('fundrasier') ) : ?>
							<p class="preheadline is-purple">Fundraiser</p>
						<?php elseif ( get_field('preheadline') ) : ?>
							<p class="preheadline <?php the_field('preheadline_color'); ?>"><?php the_field('preheadline'); ?></p>
						<?php endif; ?>
						<h2 class="<?php echo $sizeClass; ?>"><?php the_title(); ?></h2>
						<?php if ( get_field('subheadline') && get_the_ID() != '427' ) : ?>
							<p class="subheadline"><?php the_field('subheadline'); ?></p>
						<?php endif; ?>
						<?php if ( get_field('description') ) : ?>
							<p><?php the_field('description'); ?></p>
						<?php endif; ?>
						<div class="button is-blue">Learn More</div>
					</article>
				</a>
			<?php endwhile; ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h3>We cannot find anything for "<?php echo(get_search_query()); ?>".</h3>
			</section>
		</article>
	<?php endif; ?>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>